// Redeclared variable
var x = 1;
var x = 2;

// Assignment to constant
const y = null;
y = 15;
console.log(y);

// Unmatchable dollar in regular expression
if ("foo".match(/\.\(\w+$\)/))
	console.log("Found it.");

